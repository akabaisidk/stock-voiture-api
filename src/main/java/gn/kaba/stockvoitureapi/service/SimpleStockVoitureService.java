package gn.kaba.stockvoitureapi.service;

import gn.kaba.stockvoitureapi.dto.StockVoitureResponse;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class SimpleStockVoitureService implements StockVoitureService {
    @Value("${server.port}")
    private String port;
    @Override
    public ResponseEntity<StockVoitureResponse> getStockVoiture() {
        return ResponseEntity.ok(StockVoitureResponse
                .builder()
                .libelle("Toyota Rav 4")
                .prix(5000000.)
                .description("Essence 6 cylindres")
                .port(port)
                .build()
        );
    }
}
