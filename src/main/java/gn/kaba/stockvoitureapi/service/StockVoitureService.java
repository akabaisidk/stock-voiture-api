package gn.kaba.stockvoitureapi.service;

import gn.kaba.stockvoitureapi.dto.StockVoitureResponse;
import org.springframework.http.ResponseEntity;

public interface StockVoitureService {
    ResponseEntity<StockVoitureResponse> getStockVoiture();
}
