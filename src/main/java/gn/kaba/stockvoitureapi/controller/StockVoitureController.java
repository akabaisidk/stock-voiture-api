package gn.kaba.stockvoitureapi.controller;

import gn.kaba.stockvoitureapi.dto.StockVoitureResponse;
import gn.kaba.stockvoitureapi.service.StockVoitureService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class StockVoitureController {

    private final StockVoitureService stockVoitureService;

    public StockVoitureController(StockVoitureService stockVoitureService) {
        this.stockVoitureService = stockVoitureService;
    }

    @GetMapping("/voitures")
    ResponseEntity<StockVoitureResponse> getStockVoiture(){

        return stockVoitureService.getStockVoiture();
    }
}
