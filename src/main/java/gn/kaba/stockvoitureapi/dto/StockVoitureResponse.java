package gn.kaba.stockvoitureapi.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class StockVoitureResponse {
    private String libelle;
    private Double prix;
    private String description;
    private String port;
}
