package gn.kaba.stockvoitureapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@EnableEurekaClient
@SpringBootApplication
public class StockVoitureApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(StockVoitureApiApplication.class, args);
	}

}
